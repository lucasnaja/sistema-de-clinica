# Sistema de Clínica

### Autor: Lucas Bittencourt

### Tecnologias usadas
- PHP
- SQL
- MaterializeCSS

## Imagens

### Registro
![img](assets/images/registro.png "Registro")
### Login
![img](assets/images/login.png "Tela de Login")
### Home
![img](assets/images/home.png "Home")
### Usuários
![img](assets/images/usuarios.png "Usuários")
### Pacientes
![img](assets/images/pacientes.png "Pacientes")
### Médicos
![img](assets/images/medicos.png "Médicos")
### Formas de Pagamento
![img](assets/images/formas_de_pagamento.png "Formas de Pagamento")
### Convênios
![img](assets/images/convenios.png "Convênios")
### Agenda
![img](assets/images/agenda.png "Agenda")
### Agenda parte 2
![img](assets/images/agenda_2.png "Agenda 2")
