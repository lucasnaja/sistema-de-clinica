<?php
try {
   include_once('../../assets/conexao.php');

   $pac_nome = filter_input(INPUT_POST, 'pac_nome', FILTER_DEFAULT);

   $sql = $pdo->prepare("INSERT INTO pacientes (pac_nome) VALUE (:pac_nome)");

   $sql->bindValue(':pac_nome', $pac_nome);
   $sql->execute();
   
   header('Location: form_pacientes.php');
} catch (PDOException $e) {
   echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
