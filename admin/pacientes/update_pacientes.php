<?php
try {
   include_once('../../assets/conexao.php');

   $pac_id = filter_input(INPUT_POST, 'pac_id', FILTER_DEFAULT);
   $pac_nome = filter_input(INPUT_POST, 'pac_nome', FILTER_DEFAULT);

   $sql = $pdo->prepare("UPDATE pacientes SET pac_nome=:pac_nome WHERE pac_id=:pac_id");

   $sql->bindValue(':pac_nome', $pac_nome);
   $sql->bindValue(':pac_id', $pac_id);
   $sql->execute();

   header('Location: form_pacientes.php');
} catch (PDOException $e) {
   echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
