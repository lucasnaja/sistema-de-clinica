<?php
try {
   include_once('../../assets/conexao.php');

   $con_id = filter_input(INPUT_POST, 'con_id', FILTER_DEFAULT);
   $con_nome = filter_input(INPUT_POST, 'con_nome', FILTER_DEFAULT);
   $con_status = filter_input(INPUT_POST, 'con_status', FILTER_DEFAULT);

   $sql = $pdo->prepare("UPDATE convenios SET con_nome=:con_nome, con_status=:con_status WHERE con_id=:con_id");

   $sql->bindValue(':con_nome', $con_nome);
   $sql->bindValue(':con_status', $con_status);
   $sql->bindValue(':con_id', $con_id);

   $sql->execute();

   header('Location: form_convenio.php');
} catch (PDOException $e) {
   echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
