<?php
try {
   include_once('../../assets/conexao.php');

   $con_nome = filter_input(INPUT_POST, 'con_nome', FILTER_DEFAULT);

   $sql = $pdo->prepare("INSERT INTO convenios (con_nome) VALUES (:con_nome)");

   $sql->bindValue(':con_nome', $con_nome);
   $sql->execute();
   
   header('Location: form_convenio.php');
} catch (PDOException $e) {
   echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
