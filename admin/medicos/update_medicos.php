<?php
try {
   include_once('../../assets/conexao.php');

   $med_id = filter_input(INPUT_POST, 'med_id', FILTER_DEFAULT);
   $med_nome = filter_input(INPUT_POST, 'med_nome', FILTER_DEFAULT);

   $sql = $pdo->prepare("UPDATE medicos SET med_nome=:med_nome WHERE med_id=:med_id");

   $sql->bindValue(':med_nome', $med_nome);
   $sql->bindValue(':med_id', $med_id);
   $sql->execute();

   header('Location: form_medicos.php');
} catch (PDOException $e) {
   echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
