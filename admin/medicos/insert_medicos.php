<?php
try {
   include_once('../../assets/conexao.php');

   $med_nome = filter_input(INPUT_POST, 'med_nome', FILTER_DEFAULT);

   $sql = $pdo->prepare("INSERT INTO medicos (med_nome) VALUE (:med_nome)");

   $sql->bindValue(':med_nome', $med_nome);
   $sql->execute();
   
   header('Location: form_medicos.php');
} catch (PDOException $e) {
   echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
