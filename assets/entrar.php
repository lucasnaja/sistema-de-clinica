<?php
try {
   include_once('conexao.php');

   $usu_email = filter_input(INPUT_POST, 'usu_email', FILTER_DEFAULT);
   $usu_senha = filter_input(INPUT_POST, 'usu_senha', FILTER_DEFAULT);

   $sql = $pdo->prepare("SELECT * FROM usuarios WHERE usu_email=:usu_email and usu_senha=:usu_senha");
   
   $sql->bindValue(":usu_email", $usu_email);
   $sql->bindValue(":usu_senha", MD5($usu_senha));
   
   $sql->execute();

   $data = $sql->fetchAll();

   if ($sql->rowCount()) {
      session_start();
      $_SESSION["Login"]["email"] = $usu_email;
      $_SESSION["Login"]["nome"] = $data[0]['usu_nome'];
      header('Location: ../admin/home.php');
   } else {
      header('Location: ../');
   }
} catch (PDOException $e) {
   echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
