-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 14, 2019 at 05:48 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Clinica_CRUD`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `age_id` int(11) NOT NULL,
  `age_data` date NOT NULL,
  `age_horario` time NOT NULL,
  `med_id` int(11) NOT NULL,
  `pac_id` int(11) NOT NULL,
  `con_id` int(11) NOT NULL,
  `for_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`age_id`, `age_data`, `age_horario`, `med_id`, `pac_id`, `con_id`, `for_id`) VALUES
(19, '2019-06-13', '06:44:00', 9, 7, 8, 13),
(20, '2019-06-14', '06:44:00', 9, 8, 8, 13),
(21, '2019-06-15', '06:44:00', 9, 9, 8, 15),
(22, '2019-06-16', '06:44:00', 9, 10, 8, 13),
(23, '2019-06-02', '06:44:00', 10, 7, 9, 14),
(24, '2019-06-03', '06:45:00', 10, 8, 9, 15),
(25, '2019-06-05', '06:45:00', 10, 9, 8, 14),
(26, '2019-06-07', '06:45:00', 10, 10, 9, 15),
(27, '2019-06-23', '07:21:00', 11, 7, 8, 13),
(28, '2019-06-24', '07:21:00', 11, 8, 9, 14),
(29, '2019-06-25', '07:21:00', 11, 9, 8, 15),
(30, '2019-06-26', '07:21:00', 11, 10, 8, 13);

-- --------------------------------------------------------

--
-- Table structure for table `convenios`
--

CREATE TABLE `convenios` (
  `con_id` int(11) NOT NULL,
  `con_nome` varchar(45) NOT NULL,
  `con_status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `convenios`
--

INSERT INTO `convenios` (`con_id`, `con_nome`, `con_status`) VALUES
(8, 'SUS', 1),
(9, 'Unimed', 1),
(10, 'Petrobrás', 0);

-- --------------------------------------------------------

--
-- Table structure for table `formas_pagamento`
--

CREATE TABLE `formas_pagamento` (
  `for_id` int(11) NOT NULL,
  `for_nome` varchar(255) NOT NULL,
  `for_status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `formas_pagamento`
--

INSERT INTO `formas_pagamento` (`for_id`, `for_nome`, `for_status`) VALUES
(13, 'Dinheiro', 1),
(14, 'Boleto', 1),
(15, 'Cartão de Crédito', 1),
(16, 'Ouro', 0);

-- --------------------------------------------------------

--
-- Table structure for table `medicos`
--

CREATE TABLE `medicos` (
  `med_id` int(11) NOT NULL,
  `med_nome` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `medicos`
--

INSERT INTO `medicos` (`med_id`, `med_nome`) VALUES
(9, 'Lucas Bittencourt'),
(10, 'Rodrigo Dionísio'),
(11, 'Nelci Mariano');

-- --------------------------------------------------------

--
-- Table structure for table `pacientes`
--

CREATE TABLE `pacientes` (
  `pac_id` int(11) NOT NULL,
  `pac_nome` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pacientes`
--

INSERT INTO `pacientes` (`pac_id`, `pac_nome`) VALUES
(7, 'Luís Romão'),
(8, 'Vinícius Job'),
(9, 'Suzany Silva'),
(10, 'Renan Mattos'),
(11, 'Stheffany');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `usu_id` int(11) NOT NULL,
  `usu_nome` varchar(45) NOT NULL,
  `usu_email` varchar(45) NOT NULL,
  `usu_senha` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`usu_id`, `usu_nome`, `usu_email`, `usu_senha`) VALUES
(19, 'Lucas Bittencourt', 'lucas@gmail.com', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`age_id`),
  ADD KEY `medicos_fk` (`med_id`) USING BTREE,
  ADD KEY `pacientes_fk` (`pac_id`) USING BTREE,
  ADD KEY `formas_pagamento_fk` (`for_id`) USING BTREE,
  ADD KEY `convenios_fk` (`con_id`) USING BTREE;

--
-- Indexes for table `convenios`
--
ALTER TABLE `convenios`
  ADD PRIMARY KEY (`con_id`);

--
-- Indexes for table `formas_pagamento`
--
ALTER TABLE `formas_pagamento`
  ADD PRIMARY KEY (`for_id`);

--
-- Indexes for table `medicos`
--
ALTER TABLE `medicos`
  ADD PRIMARY KEY (`med_id`);

--
-- Indexes for table `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`pac_id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usu_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `age_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `convenios`
--
ALTER TABLE `convenios`
  MODIFY `con_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `formas_pagamento`
--
ALTER TABLE `formas_pagamento`
  MODIFY `for_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `medicos`
--
ALTER TABLE `medicos`
  MODIFY `med_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `pac_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `agenda`
--
ALTER TABLE `agenda`
  ADD CONSTRAINT `covenant_fk` FOREIGN KEY (`con_id`) REFERENCES `convenios` (`con_id`),
  ADD CONSTRAINT `medic_fk` FOREIGN KEY (`med_id`) REFERENCES `medicos` (`med_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pacient_fk` FOREIGN KEY (`pac_id`) REFERENCES `pacientes` (`pac_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `payments_fk` FOREIGN KEY (`for_id`) REFERENCES `formas_pagamento` (`for_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
