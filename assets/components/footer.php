<footer class="page-footer indigo darken-4">
   <div class="container">
      <div class="row">
         <div class="col l6 s12">
            <h5 class="white-text">Sobre</h5>
            <p>Sistema de Clínica - Etec <?= date('Y') ?></p>
         </div>
         <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Links</h5>
            <ul>
               <li><a class="grey-text text-lighten-3" href="https://github.com/lucasnaja" target="_blank" rel="noopener noreferrer nofollow">GitHub</a></li>
               <li><a class="grey-text text-lighten-3" href="https://www.linkedin.com/in/lucas-bittencourt/" target="_blank" rel="noopener noreferrer nofollow">LinkedIn</a></li>
               <li><a class="grey-text text-lighten-3" href="https://dev.to/lucasnaja" target="_blank" rel="noopener noreferrer nofollow">Dev Community</a></li>
               <li><a class="grey-text text-lighten-3" href="https://twitter.com/lucasnaja0" target="_blank" rel="noopener noreferrer nofollow">Twitter</a></li>
            </ul>
         </div>
      </div>
   </div>

   <div class="footer-copyright">
      <div class="container">
         <?php
         if (isset($_SESSION['Login']['nome']) && isset($_SESSION['Login']['email'])) {
            $nome = $_SESSION['Login']['nome'];
            $email = $_SESSION['Login']['email'];
         }
         ?>
         © Copyright 2019 - <?= isset($nome) ? $nome : 'Guaratinguetá' ?> - <?= isset($email) ? $email : 'Brasil' ?>
      </div>
   </div>
</footer>
